/*******************************************************************
  Project for MicroZed based MZ_APO board
  designed by Robert Popelka and Alexandr Silchenko

  connect4_main.c      - main file
  pvp_module.c         - player vs player game engine
  pvp_module.c         - player vs ai game engine
  ai_engine.c          - ai logic
  graf_engine.c        - lcd display rendering
  peripheries.c        - functions for using MZ_APO board peripheries

    (C) Copyright 2022 by Robert Popelka and Alexandr Silchenko
      e-mail:   popelrob@fel.cvut.cz, silchale@fel.cvut.cz
      license:  any combination of GPL, LGPL, MPL or BSD licenses

 *******************************************************************/

#include "pvp_module.h"
#include "pve_module.h"
#include "graf_engine.h"
#include "peripheries.h"
#include "defines.h"
#include "serialize_lock.h"

int main() {

    /* Serialize execution of applications */

    /* Try to acquire lock the first */
    if (serialize_lock(1) <= 0) {
        printf("System is occupied\n");

        if (1) {
            printf("Waitting\n");
            /* Wait till application holding lock releases it or exits */
            serialize_lock(0);
        }
    }

    display_init();
    knobs_init();

    draw_menu();        // render main menu
    change_led_color(LED_BLUE);
    activate_led_line(3);       // start signaling

    bool quit = false;
    bool draw = false;
    uint8_t select;

    while(!quit) {
        draw = true;
        select = blue_knob_turn() % 3;      // use data from blue knob to navigate the menu
        draw_arrow(select);
        if (is_knob_pressed() == 3) {       // if blue knob pressed select current menu icon
            switch (select) {
                case 0:
                    draw = !pvp_engine();   // start pvp game
                    if (draw) {
                        draw_draw();
                    }
                    activate_led_line(6);   // signal end of game
                    draw_menu();
                    change_led_color(LED_BLUE);
                    break;
                case 1:
                    draw = !pve_engine();   // start pve game
                    if (draw) {
                        draw_draw();
                    }
                    activate_led_line(6);   // signal end of game
                    draw_menu();
                    change_led_color(LED_BLUE);
                    break;
                case 2:
                    quit = true;    // quit program and render the display black
                    color_and_draw_display(BLACK);
                    change_led_color(LED_BLACK);
                    break;
                default:
                    break;
            }
        }
    }
    /* Release the lock */
    serialize_unlock();
  return 0;
}
