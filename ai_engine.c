//
// Created by robert on 17.5.22.
//
#include "ai_engine.h"

int minmax(char *game_state) {
    char player_turn = 'x';
    int curr_score;
    int best_score = -100;
    int best_col;
    bool possible_moves = true;
    if (!possible_moves) {
        return -1;
    }
    for (int col_pointer = 0; col_pointer < 7; ++col_pointer) {     // pick move with the best score
        for (int i = MAX_ROW - 1; i >= 0; --i) {
            if (game_state[MAX_COL * i + col_pointer] == 0) {
                game_state[MAX_COL * i + col_pointer] = player_turn;
                curr_score = get_score(game_state, player_turn);
                if (curr_score > best_score) {
                    best_score = curr_score;
                    best_col = col_pointer;
                }
                game_state[MAX_COL * i + col_pointer] = 0;
                break;
            }
        }
    }
    return best_col;
}

int get_score(const char *game_state, char player_turn) {
    char opponent_turn = (player_turn == 'x') ? 'o' : 'x';
    char prev_horiz = ' ';
    int prev_vert[MAX_COL] = {0, 0, 0, 0, 0, 0, 0};
    int score = 0;
    for (int i = 0; i < BOARD_SIZE - 1; ++i) {      // amplify score by being on top
        if (prev_vert[i % MAX_COL] == 0) {
            if (game_state[i] == opponent_turn) {
                prev_vert[i % MAX_COL] -= 1;
            }
            if (game_state[i] == player_turn) {
                prev_vert[i % MAX_COL] += 1;
            }
        }
        if ((i + 1) % MAX_COL != 0) {   // increase score by having multiple tokens horizontally aligned
            if (game_state[i] == game_state[i + 1] && game_state[i] == player_turn) {
                score += 1;
                if (prev_horiz == player_turn) {
                    score += 2;
                }
                prev_horiz = player_turn;
            } else if (game_state[i] == game_state[i + 1] && game_state[i] == opponent_turn) {
                score -= 1;
                if (prev_horiz == opponent_turn) {
                    score -= 2;
                }
                prev_horiz = opponent_turn;
            } else {
                prev_horiz = ' ';
            }
        }
        if (i < BOARD_SIZE - MAX_COL) {     // increase score by having multiple tokens vertically aligned
            if (game_state[i] == game_state[i + MAX_COL] && game_state[i] == player_turn) {
                score += 1 + prev_vert[i % MAX_COL];
            } else if (game_state[i] == game_state[i + MAX_COL] && game_state[i] == opponent_turn) {
                score -= 1 - prev_vert[i % MAX_COL];
            }
            if (i % MAX_COL != 6) {     // increase score by having multiple tokens diagonally aligned
                if (game_state[i] == game_state[i + MAX_COL + 1] && game_state[i] == player_turn) {
                    score += 1 + prev_vert[(i - 1) % MAX_COL];
                } else if (game_state[i] == game_state[i + MAX_COL + 1] && game_state[i] == opponent_turn) {
                    score -= 1 - prev_vert[(i - 1) % MAX_COL];
                }
            }
            if (i % MAX_COL != 0) {     // increase score by having multiple tokens diagonally aligned
                if (game_state[i] == game_state[i + MAX_COL - 1] && game_state[i] == player_turn) {
                    score += 1 + prev_vert[(i + 1) % MAX_COL];
                } else if (game_state[i] == game_state[i + MAX_COL - 1] && game_state[i] == opponent_turn) {
                    score -= 1 - prev_vert[(i + 1) % MAX_COL];
                }
            }
        }
    }
    return score;
}