//
// Created by robert on 7.5.22.
//

#ifndef APO_PVP_MODULE_H
#define APO_PVP_MODULE_H

#include "graf_engine.h"
#include "peripheries.h"
#include "defines.h"

bool pvp_engine();
// runs the pvp engine, returns false if the match results in a draw, true otherwise
bool play_turn(char *game_state, char player_turn, uint8_t *col_buff);
// takes player input and changes game state
bool check_four(const char *game_state, int a, int b, int c, int d);
// checks four specified positions on board and return bool if win
bool horizontal_check(char *game_state);
// checks horizontal win
bool vertical_check(char *game_state);
// checks vertical win
bool diagonal_check(char *game_state);
// checks diagonal win
bool check_win(char *game_state);
// checks win condition functions

#endif //APO_PVP_MODULE_H
