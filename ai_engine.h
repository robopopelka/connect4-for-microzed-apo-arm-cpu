//
// Created by robert on 17.5.22.
//

#ifndef APOSEM_ALPHABETA_H
#define APOSEM_ALPHABETA_H

#include "pvp_module.h"
#include "defines.h"

int minmax(char *game_state);
// calculates next move with the most value
int get_score(const char *game_state, char player_turn);
// returns value of the current board position for the given player

#endif //APOSEM_ALPHABETA_H
