   _____                            _     ______
  / ____|                          | |   |  ____|
 | |     ___  _ __  _ __   ___  ___| |_  | |__ ___  _   _ _ __
 | |    / _ \| '_ \| '_ \ / _ \/ __| __| |  __/ _ \| | | | '__|
 | |___| (_) | | | | | | |  __/ (__| |_  | | | (_) | |_| | |
  \_____\___/|_| |_|_| |_|\___|\___|\__| |_|  \___/ \__,_|_|

    Connect four is a two player adversarial game in which the players take turns placing their respective tokens on
the 6 x 7 playing board. The token can only be placed on the first open lowest tile in one of the 7 columns on the
board. (in our program this is automatic and a player can only select a column in which they want to place their
token). The goal of the game is to place 4 tokens adjacent to each other either vertically, horizontally or
diagonally. The game can end in 3 different ways: one of the players wins, one of the players quits the game, or the
whole game board ends up full without either player winning which results in a draw.

    Connect 4 on MZAPO board

    To launch the game on the MZAPO board, compilation for the ARM Linux system is required. The Makefile in this
folder handles the compilation. If the compilation fails check if any source files are missing.
    The list of source files:
        - connect4_main.c
        - mzapo_phys.c
        - mzapo_parlcd.c
        - serialize_lock.c
        - pvp_module.c
        - pve_module.c
        - ai_engine.c
        - graf_engine.c
        - peripheries.c
        - font_prop14x16.c
        - font_rom8x16.c

    The compiler will create an EXE file named connect4. After you run this file on the MZAPO board the main menu
will show up on the LCD display and the LED strip on the board will light up rapidly in order to signal the startup
sequence. While the main menu is displayed two RGB LED diodes on the board are shining in blue. Our implementation
of the game has two game modes: PVP and PVE. Together with the QUIT icon there are three choices the user can choose
by rotating the blue knob on the board which will move an arrow pointer to one of the icons. To select an option
press the blue knob.

    In the PVP game mode two players take turns to play against each other. The player tokens are X and O and are
colored red and green respectively. While a player is taking a turn the RGB diodes shine in their color and one of
the playing board tiles is colored in their color to signal the currently selected tile. The player can change their
selected tile by rotating the knob of their respective color and lock in their choice by pressing it. This will place
their token on the selected tile and the other player gets their turn. To quit the game either player can press the
blue knob, after which the game will end by showing a PLAYER X (or O) QUIT! screen and the diodes shining in a neutral
color. If a player attempts to play an illegal move it will be signaled by the same diode color. After either player
aligns 4 of their tokens next to each other they will be highlighted in yellow and the RGB diodes will shine in the
same color. The LED strip will activate similarly to the startup sequence and after a short while winner screen will
show up momentarily before returning to the main menu. In the PVE mode the player plays against an AI opponent in the
same way as the PVP mode. If QUIT icon is selected in the main menu the LCD display will turn black and the program
will cease running.

    Used peripheries

    - LCD display -> showing the game menu as well as the game itself
    - LED strip -> signaling the startup of the program and end of the game
    - RGB LED diodes -> shining BLUE while in the game menu, RED or GREEN signaling a players turn, PINK in case of
        invalid turn or signaling a player leaving the game, YELLOW to signal the end of the game
    - BLUE knob -> used for selecting an option from the main menu or leaving the game
    - RED knob -> used to control the placement of tokens for the X player
    - GREEN knob -> used to control the placement of tokens for the O player

    Modules

    - connect4_main.c -> main file responsible for controlling the main menu and selecting a game mode
    - pvp_module.c -> PVP game mode engine responsible for players playing their turns as well as checking win
        conditions
    - pve_module.c -> PVE game mode engine responsible for calling the AI functions from ai_engine.c or player
        turn and win condition functions from the pvp_module.c
    - ai_engine.c -> AI module for playing a turn selected as best by the get_score function
    - graf_engine.c -> module responsible for rendering all the graphics on the LCD display
    - peripheries.c -> module containing functions for using the knobs, RGB diodes and the LED strip
    - serialize_lock.c -> responsible for serializing execution of applications
    - mzapo_phys.c -> resposible for mapping the physical address of the peripheries
    - mzapo_parlcd.c -> provides functions for using the LCD display
    - font_prop14x16.c -> font for text rendering
    - font_rom8x16.c -> font for text rendering

    More info about specific functions and their use can be found in the header files of the respective modules.
